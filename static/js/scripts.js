var today = new Date();
var year = today.getFullYear();
var month = today.getMonth();
var day = today.getDate();

console.log(today);

var chart1Data = [
    {
    date: new Date(year, month, day - 20),
    visits: 5120,
    views: 16734},
    {
    date: new Date(year, month, day - 19),
    visits: 5040,
    views: 16320},
    {
    date: new Date(year, month, day - 18),
    visits: 4823,
    views: 15002
    },
    {
    date: new Date(year, month, day - 17),
    visits: 4252,
    views: 14288}
];


AmCharts.ready(function() {
    var chart1 = new AmCharts.AmSerialChart();
    // chart1.pathToImages = "http://www.amcharts.com/lib/images";
    chart1.dataProvider = chart1Data;
    chart1.categoryField = "date";

    var graph = new AmCharts.AmGraph();
    graph.valueField = "visits";
    chart.addGraph(graph);

    chart1.write("chartdiv1");
});
