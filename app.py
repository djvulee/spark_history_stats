# @Author: djvulee
# @Date:   2016-07-22T18:31:02+08:00
# @Last modified by:   djvulee
# @Last modified time: 2016-07-24T16:51:53+08:00


from flask import Flask, render_template, request
from pymongo import MongoClient
from datetime import date, datetime
import time
import operator
import json

client = MongoClient()
db = client["spark_online_db"]
epoch = datetime(1970, 1, 1)

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def main():
    s_date, e_date = "", ""
    if request.method == "GET":
        # print "GET"
        date_para = date.today().strftime("%Y-%m-%d")
        s_date = "2016-07-21"
        e_date = date_para
    else:
        # print "POST"
        s_date = request.form["start_date"]
        e_date = request.form["end_date"]
    #print s_date, e_date
    s_epoch = long(((datetime.strptime(s_date, "%Y-%m-%d") -
                     epoch).total_seconds() - 28800) * 1000L)
    e_epoch = long(((datetime.strptime(e_date, "%Y-%m-%d") -
                     epoch).total_seconds() + 86400 - 28800) * 1000L)
    # print s_epoch, e_epoch

    cursor = db.posts.find(
        {"app_start_time": {"$gte": s_epoch, "$lt": e_epoch}})
    # print cursor.count()

    date_dict = {}
    luigi_date_dict = {}
    user_dict = {}
    # <1min, 1-10min, 10-30min, 30-60min, 1-3h, >3h
    apps_running_time = [0, 0, 0, 0, 0, 0]
    # =1, 2-5, 5-10, 10-50, 50-100 >100
    jobs_cnt_feq = [0, 0, 0, 0, 0, 0]
    fail_reason_dict = [
      ["IOException", 0],
      ["SparkException", 0],
      ["FetchFailedException", 0],
      ["MetadataFetchFailedException", 0],
      ["CorruptedEnvironmentException", 0]
    ]
    total_cores = 0
    failed_cores = 0
    total_mem = 0
    failed_mem = 0
    for item in cursor:
        my_date = date.fromtimestamp(long(item["app_start_time"] / 1000))
        if "haruna" in item["app_name"]:
            if my_date not in luigi_date_dict:
                luigi_date_dict[my_date] = [1, 0, item["app_start_time"]]
                if item["failed"]:
                    luigi_date_dict[my_date][1] += 1
            else:
                luigi_date_dict[my_date][0] += 1
                if item["failed"]:
                    luigi_date_dict[my_date][1] += 1

        if my_date not in date_dict:
            date_dict[my_date] = [1, 0, item["app_start_time"]]
            if item["failed"]:
                date_dict[my_date][1] += 1
        else:
            date_dict[my_date][0] += 1
            if item["failed"]:
                date_dict[my_date][1] += 1


        if item["failed"]:
            failed_cores += item["exe_cores"]
            failed_mem += item["exe_mem"]

            app_name = item["app_name"].split("_")[-1]
            if app_name not in user_dict:
                user_dict[app_name] = 1
            else:
                user_dict[app_name] += 1

            find_it = False
            for key, val in item["jobs"].items():
                if find_it:
                    break
                if val[-1] != "":
                    for err in fail_reason_dict:
                        if err[0] in val[-1]:
                            err[1] += 1
                            find_it = True
                            break

                    if not find_it:
                        out_msg = "".join(val[-1].split("\n")[0].split(":")[3:]).strip()
                        fail_reason_dict.append([out_msg, 0])

        # <1min, 1-10min, 10-30min, 30-60min, 1-3h, >3h
        if item["app_end_time"] != -1:
            stime = item["app_start_time"] / 1000.0
            etime = item["app_end_time"] / 1000.0
            span = (etime - stime) / 60.0 # in minutes

            total_cores += item["exe_cores"] * span
            total_mem += item["exe_mem"] * span
            if item["failed"]:
                # we ignore the streaming apps, because the resource is not wasted
                if "streaming" in item["app_name"] or item["jobs_cnt"] > 100:
                    pass
                else:
                    failed_cores += item["exe_cores"] * span
                    failed_mem += item["exe_mem"] * span

            if span <= 1:
                apps_running_time[0] += 1
            elif span>1 and span<=10:
                apps_running_time[1] += 1
            elif span>10 and span<=30:
                apps_running_time[2] += 1
            elif span>30 and span<=60:
                apps_running_time[3] += 1
            elif span>60 and span <180:
                apps_running_time[4] += 1
            else:
                apps_running_time[5] += 1

        if not item["failed"]:
            cnt = item["jobs_cnt"]
            if cnt == 1:
                jobs_cnt_feq[0] += 1
            elif cnt>1 and cnt <= 5:
                jobs_cnt_feq[1] += 1
            elif cnt>5 and cnt <= 10:
                jobs_cnt_feq[2] += 1
            elif cnt>10 and cnt <= 50:
                jobs_cnt_feq[3] += 1
            elif cnt>50 and cnt <= 100:
                jobs_cnt_feq[4] += 1
            else:
                jobs_cnt_feq[5] += 1

    print fail_reason_dict

    cpu_resource = [
        {"app_type": "success", "core_cnt": long((total_cores - failed_cores)/60)},
        {"app_type": "failed", "core_cnt": long(failed_cores/60)}
    ]

    mem_resource = [
        {"app_type": "success", "mem_cnt": long((total_mem - failed_mem)/60)},
        {"app_type": "failed", "mem_cnt": long(failed_mem/60)}
    ]

    time_feq_x = ["<1min", "1-10min", "10-30min", "30-60min", "1-3h", ">=3h"]
    time_feq = []
    for i in xrange(6):
        time_feq.append({"time_span": time_feq_x[i], "time_value": apps_running_time[i]})
    # print time_feq


    jobs_cnt_x = ["=1", "2-5", "5-10", "10-50", "50-100", ">100"]
    jobs_feq = []
    for i in xrange(6):
        jobs_feq.append({"jobs_cnt_head": jobs_cnt_x[i], "jobs_cnt_val": jobs_cnt_feq[i]})
    # print jobs_feq



    sorted_dict = sorted(date_dict.items(), key=operator.itemgetter(0))
    sorted_luigi_dict = sorted(luigi_date_dict.items(), key=operator.itemgetter(0))
    # print sorted_dict
    # print sorted_luigi_dict

    result = []
    for item in sorted_dict:
        result.append({"date": item[1][2], "total": item[
                      1][0], "failed": item[1][1]})
    luigi_result = []
    for item in sorted_luigi_dict:
        luigi_result.append({"date": item[1][2], "total": item[
                      1][0], "failed": item[1][1]})

    user_result = []
    # print user_dict
    for key, value in user_dict.items():
        user_result.append({"name": key, "failed": value})

    exception_types = []
    for item in fail_reason_dict:
        exception_types.append({"err_type": item[0], "err_cnt": item[1]})
    print exception_types

    return render_template("index.html", start_date=s_date, end_date=e_date,
                           all_apps=json.dumps(result),
                           luigi_apps=json.dumps(luigi_result),
                           user_failed=json.dumps(user_result),
                           time_freq=json.dumps(time_feq),
                           jobs_freq=json.dumps(jobs_feq),
                           exception_types=json.dumps(exception_types),
                           cpu_resource=json.dumps(cpu_resource),
                           mem_resource=json.dumps(mem_resource)
                           )


if __name__ == "__main__":
    try:
        app.run(debug=True, host='0.0.0.0', port=12308)
    finally:
        client.close()
